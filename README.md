# Podcast Data

This repository contains:

- podcast Artworks in original sizes in `artworks/`
- some useful scripts for maintenance tasks in `scripts/`
- data processing notebooks used for initial exploration and mungling in `notebooks/`
- information about storage setup and version control of some custom edited files in `storage/`
