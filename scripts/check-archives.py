#!/usr/bin/env python3 

import requests
import xmltodict
import urllib.parse
import dateutil.parser
import json

STORAGE_HOST="https://sirimangalo-public.nyc3.cdn.digitaloceanspaces.com"
STORAGE_PREFIX="podcast-mirror"
SITEMAP_HOST='https://podcast.sirimangalo.org'
SITEMAP_URL='https://podcast.sirimangalo.org/sitemap.txt'

if __name__ == '__main__':
    marker = ''
    modified = {}
    print('Downloading S3 Metadata')

    for i in range(50):
        page = xmltodict.parse(requests.get('%s?prefix=%s&marker=%s' % (STORAGE_HOST, STORAGE_PREFIX, marker)).text)

        for item in page['ListBucketResult']['Contents']:
            modified[STORAGE_HOST + '/' + urllib.parse.quote(item['Key'])] = item['LastModified']

        if not 'NextMarker' in page['ListBucketResult']:
            break

        marker = page['ListBucketResult']['NextMarker'] 

    failed = False
    sitemap = requests.get(SITEMAP_URL).text
    for site in sitemap.split('\n'):
        if not site.strip():
            break

        feed_data = requests.get(site + 'feed-data.json').json()

        slug = site.replace(SITEMAP_HOST, '').replace('/', '')
        archive_path = '%s/%s/archives/%s.zip' % (STORAGE_HOST, STORAGE_PREFIX, slug)

        print('Checking ', archive_path)
        if not archive_path in modified:
            print('Missing Archive: ', site)
            failed = True
            continue

        archive_date = dateutil.parser.isoparse(modified[archive_path])

        new_audio_files = []
        for item in feed_data['episodes']:
            if not item['audioUrl'] in modified:
                failed = True
                print('Missing: ', item['audioUrl'])
                continue

            audio_date = dateutil.parser.isoparse(modified[item['audioUrl']])
            if audio_date > archive_date:
                new_audio_files.append(item['audioUrl'])

        if len(new_audio_files) > 0:
            print('Found new files!')
            with open(slug + '.txt', 'w') as f:
                f.write('\n'.join(new_audio_files))
    
    if failed:
        exit(1)
