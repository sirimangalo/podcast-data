#!/bin/bash

# Creates zip archives of all podcasts

STORAGE_PATH="<<INSERT>>"
STORAGE_URL="https://si-audio-files.nyc3.cdn.digitaloceanspaces.com/podcast-mirror"

slugify () {
  echo "$1" | iconv -c -t ascii//TRANSLIT | sed -r s/[~\^]+//g | sed -r s/[^a-zA-Z0-9]+/-/g | sed -r s/^-+\|-+$//g | tr A-Z a-z
}

sites=$(curl -s https://podcast.sirimangalo.org/sitemap.txt)

while read -r site; do
  data=$(curl -s "${site}feed-data.json")

  urls=$(echo "$data" | jq -r '.episodes | .[].audioUrl')
  titles=$(echo "$data" | jq -r '.episodes | .[].title')
  podcast_slug=${site#'https://podcast.sirimangalo.org/'}
  podcast_slug=${podcast_slug::-1}
  archive="${STORAGE_PATH}/archives/${podcast_slug}.zip"

  echo "Archiving ${podcast_slug}"

  episodes=$(echo "${data}" | jq '.episodes | reverse')
  archive_list=''

  for k in $(echo "${episodes}" | jq '. | keys | .[]'); do
    title=$(echo "${episodes}" | jq -r ".[$k].title")
    url=$(echo "${episodes}" | jq -r ".[$k].audioUrl" | sed 's/%20/ /g') # fix whitespace encoding
    extension="${url##*.}"

    local_file=${url#"${STORAGE_URL}"}
    local_file="${STORAGE_PATH}${local_file}"
    local_file_base=$(basename "${local_file}")
    local_file_size=$(stat -c '%s' "${local_file}")

    # e.g., "115_mark-zuckerberg-vegetarianism-and-killing.mp3"
    friendly_filename=$(slugify "$title")
    friendly_filename="$(printf "%03d" $((k+1)))_${friendly_filename}.${extension}"

    if [ -z "${archive_list}" ] && [ -f "${archive}" ]; then
      archive_list=$(unzip -l "${archive}")
    fi

    echo "${archive_list}" | grep "${friendly_filename}" | grep "${local_file_size}" >/dev/null 2>&1
    ret=$?

    if [ $ret -ne 0 ]; then
      # add file to archive and change name to be friendly 
      zip -9 -j "${archive}" "${local_file}"
      printf "@ ${local_file_base}\n@=${friendly_filename}\n" | zipnote -w "${archive}"
    fi
  done
done <<< "${sites}"
