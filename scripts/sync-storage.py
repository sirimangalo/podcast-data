#!/usr/bin/env python3 

import traceback
import requests
import xmltodict
import urllib.parse
import urllib.request
from os import path, makedirs, remove, rename

STORAGE_HOST="https://sirimangalo-public.nyc3.cdn.digitaloceanspaces.com"
STORAGE_PREFIX="podcast-mirror"

import hashlib

def get_etag(fname, chunk_size=15 * 1024 * 1024, buffer_size=1024):
    hashes = []
    hash_md5 = hashlib.md5()
    fragments = 0

    with open(fname, "rb") as f:
        bytes_read = 0
        for buffer in iter(lambda: f.read(buffer_size), b""):
            hash_md5.update(buffer)
            bytes_read += buffer_size

            if bytes_read >= chunk_size:
                hashes.append(hash_md5)
                hash_md5 = hashlib.md5()
                bytes_read = 0
                fragments += 1
    
    hashes.append(hash_md5)
    fragments += 1

    if fragments == 1:
        return '"%s"' % (hashes[0].hexdigest())

    final_hash = hashlib.md5()
    for hash in hashes:
        final_hash.update(hash.digest())
    
    etag = '"%s%s"' % (final_hash.hexdigest(), '-%i' % (fragments))

    return etag

def check_file(fn, md5sum):
    return path.exists(fn) and get_etag(fn) == md5sum

if __name__ == '__main__':
    marker = ''
    s3_metadata = []

    for i in range(50):
        page = xmltodict.parse(requests.get('%s?prefix=%s&marker=%s' % (STORAGE_HOST, STORAGE_PREFIX, marker)).text)
        s3_metadata += page['ListBucketResult']['Contents']

        if not 'NextMarker' in page['ListBucketResult']:
            break

        marker = page['ListBucketResult']['NextMarker'] 
    
    for item in s3_metadata:
        try:
            already_exists = 'ETag' in item and item['ETag'] and check_file(item['Key'], item['ETag'])
            if already_exists:
                print('%s Skipping' % (item['Key']))
            else:
                print('%s Downloading' % (item['Key']))
                url = '%s/%s' % (STORAGE_HOST, item['Key'])

                if not path.isdir(path.dirname(item['Key'])):
                    makedirs(path.dirname(item['Key']))

                if path.exists(item['Key']):
                    rename(item['Key'], item['Key'] + '.old')
                    
                urllib.request.urlretrieve(url, item['Key'])

                if path.exists(item['Key'] + '.old'):
                    remove(item['Key'] + '.old')
        except Exception as e:
            failed = True
            print('%s FAILED' % (item['Key']))
            print(e)
            print(traceback.format_exc())

            if path.exists(item['Key'] + '.old'):
                rename(item['Key'] + '.old', item['Key'])

    failed = False
    
    if failed:
        exit(1)
