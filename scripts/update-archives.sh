#!/bin/bash

set -u

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
STORAGE_URL="https://sirimangalo-public.nyc3.cdn.digitaloceanspaces.com/podcast-mirror"

tmpdir="/tmp/$(date --iso=ns | sha1sum | cut -d' ' -f1)"

echo "Entering ${tmpdir}"
mkdir "${tmpdir}"
cd "${tmpdir}"

python3 "${DIR}/check-archives.py"

for f in *.txt; do
  slug="${f%".txt"}"
  [[ -n "${slug}" ]] || continue
  archive_url="${STORAGE_URL}/archives/${slug}.zip"
  mkdir -p "${tmpdir}/${slug}"
  cd "${tmpdir}/${slug}"
  wget "${archive_url}" -O "${slug}.zip"
  cat "../$f" | xargs wget
  find . -name "*.mp3" -exec zip -9 -j "${slug}.zip" "{}" \;
  s3cmd --access_key="${ACCESS_KEY}" --secret_key="${SECRET_KEY}" put -n -P "${slug}.zip" "s3://sirimangalo-public/podcast-mirror/archives/${slug}.zip"
  cd "${tmpdir}/${slug}"
  find . -name "*.mp3" -exec rm {} \;
  rm "${slug.zip}"
  cd "${tmpdir}"
  rmdir "${slug}"
done
