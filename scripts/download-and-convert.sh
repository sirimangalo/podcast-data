#!/bin/bash

set -e

if [[ $# -eq 0 ]] ; then
  echo "Usage: $0 <youtube link or id>"
  exit 0
fi

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
TEMP_DIR="/tmp"
DURATIONS_JSON="${DIR}/../storage/durations.json"

# Download video from youtube, convert it to mp3 and add metadata

temp_dir_id=$(date +%s%3N)
temp_dir="${TEMP_DIR}/audio-downloader-${temp_dir_id}"
mkdir -p "${temp_dir}"

# download
echo "-> Downloading $1"
yt-dlp \
  --no-progress \
  --add-metadata \
  --write-info-json \
  --extract-audio \
  -o "${temp_dir}/%(id)s.%(ext)s" -- "$1"

audio_orig=$(find "$temp_dir" -type f ! -name "*.json")
audio_mp3="${audio_orig%.*}.mp3"
meta_json=$(find "$temp_dir" -type f -name "*.json")

# convert
echo "-> Converting to mp3"
ffmpeg \
  -hide_banner \
  -loglevel warning \
  -i "$audio_orig" \
  -nostdin \
  -b:a 96k \
  -map_metadata 0:s:0 \
  -f mp3 - > "$audio_mp3"

# set id3 tags
echo "-> Setting ID3 tags"
title=$(cat $meta_json | jq -r '.title')
artist=$(cat $meta_json | jq -r '.uploader')
year=$(cat $meta_json | jq -r '.upload_date' | head -c4)
video_id=$(cat $meta_json | jq -r '.title')

id3v2 -t "$title" "$audio_mp3"
id3v2 -a "$artist" "$audio_mp3"
id3v2 -y "$year" "$audio_mp3"
id3v2 -c "youtu.be/$video_id" "$audio_mp3"

if [[ -n "${S3_ACCESS_KEY}" ]] && [[ -n "${S3_SECRET_KEY}" ]]; then
  echo "Uploading to S3..."
  s3cmd \
    --access_key="${S3_ACCESS_KEY}" \
    --secret_key="${S3_SECRET_KEY}" \
    --region=nyc3 \
    --host=digitaloceanspaces.com \
    put -P "${audio_mp3}" s3://sirimangalo-public/podcast-mirror/
fi

# move to current dir
echo "-> Moving to current directory"
mv $audio_mp3 .
rm -rf "${temp_dir}"

filename=$(basename "$audio_mp3")

if [[ "$UPDATE_DURATIONS" -eq "1" ]]; then
  # update durations mapping
  echo "-> Updating durations.json"
  duration=$(ffprobe -i $filename -show_entries format=duration -v quiet -of csv="p=0")
  duration=${duration%.*}
  file_size=$(stat -c %s $filename)
  cat "$DURATIONS_JSON" | jq \
    --ascii-output \
    --arg NAME "$filename" \
    --arg SIZE "$file_size" \
    --arg DURATION "$duration" \
    '.[$NAME] = { ($SIZE): ($DURATION|tonumber) }' > .durations_temp.json

  mv .durations_temp.json "$DURATIONS_JSON"

  git pull # TODO: better to use fetch?
  git commit -m "feat: update durations.json"
fi

echo "-> Done!"
echo ""
