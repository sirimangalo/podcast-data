# Scripts

## check-storage.py

Checks all files in feeds if they are also available in the s3 metadata.

### Requirements

```
pip install requests xmltodict
```

## crop-artwork.sh

Script for cropping and naming artworks.

### Requirements

- ImageMagick
- jpegoptim

### Usage

```sh
./crop-artwork.sh <image path> <slug name>
```

## download-and-convert.sh

Script for

- Downloading a youtube video
- Converting it into MP3 / 128kbps (CBR)
- Setting ID3-Tags from video metadata automatically

### Requirements

- ffmpeg
- python3
- youtube-dl
- jq
- id3v2

### Usage

```sh
./download-and-convert.sh <youtube link>
```

## Download of static files

To download the entire public files at https://static.sirimangalo.org/ or https://share.sirimangalo.org/ , you can use the following command:

```
wget -np -r -nH -R "index.html*" -w 3 https://static.sirimangalo.org/ > ../download-logs.txt 2>&1
```
