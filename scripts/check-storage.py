#!/usr/bin/env python3 

import requests
import xmltodict
import urllib.parse

STORAGE_HOST="https://sirimangalo-public.nyc3.cdn.digitaloceanspaces.com"
STORAGE_PREFIX="podcast-mirror"
SITEMAP_URL='https://podcast.sirimangalo.org/sitemap.txt'

if __name__ == '__main__':
    marker = ''
    s3_sizes = {}
    for i in range(50):
        page = xmltodict.parse(requests.get('%s?prefix=%s&marker=%s' % (STORAGE_HOST, STORAGE_PREFIX, marker)).text)

        for item in page['ListBucketResult']['Contents']:
            s3_sizes[STORAGE_HOST + '/' + urllib.parse.quote(item['Key'])] = item['Size']

        if not 'NextMarker' in page['ListBucketResult']:
            break

        marker = page['ListBucketResult']['NextMarker'] 
    

    failed = False
    sitemap = requests.get(SITEMAP_URL).text
    feed_sizes = {}
    for site in sitemap.split('\n'):
        if not site.strip():
            break

        feed_data = requests.get(site + 'feed-data.json').json()

        if not feed_data['imageUrl'] in s3_sizes:
            failed = True
            print('Missing: ', feed_data['imageUrl'])

        for item in feed_data['episodes']:
            if not item['audioUrl'] in s3_sizes:
                failed = True
                print('Missing: ', item['audioUrl'])
            elif s3_sizes[item['audioUrl']] != item['audioSize']:
                failed = True
                print('Invalid Size: ' + item['audioUrl'] + ' | feed: ' + str(item['audioSize']) + ', actual: ' + s3_sizes[item['audioUrl']])

    
    if failed:
        exit(1)
