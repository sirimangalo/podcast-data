#!/bin/bash

if [[ $# -eq 0 ]] ; then
  echo "Usage: $0 <original artwork file> <slug / name>"
  exit 0
fi

IMAGE="$1"
SLUG="$2"

convert "$IMAGE" "artwork-${SLUG}.jpg"
convert -resize 300x300 "$IMAGE" "artwork-300x300-${SLUG}.jpg"
jpegoptim -S 490 "artwork-${SLUG}.jpg"
