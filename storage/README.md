# Storage

The `data/` directory reflects the structure of the cloud storage as well as contains files that are not on youtube or the static file servers or have been edited.

## Folder Structure

- Root: Contains youtube audio files using the format `<youtube-id>.mp3`
- `artworks/`: Contains artworks in format `artwork-<name>.(jpg|png)` (size-optimized, min. 2000x2000) as well as 300x300 format `artowrk-300x300-<name>.(jpg|png)` (same `<name>` must use same extension for both files)
- `broadcast/`: Files from https://share.sirimangalo.org/broadcast/
- `diraudio/`: Files from https://static.sirimangalo.org/diraudio/
- `audio-booklets/<2-letter-language-code>/`: Recordings in mp3 in name format `how-to-meditate_<language-name>-chapter-<number>.mp3`


```
.
├── artworks
├── audio-booklets
└── broadcast
```

## Setup

```
pip3 install -U yq
```

### Initial Upload

**Following is a description of how files were uploaded initially. This is mainly historical documentation and cloning/syncing the existing storage should be the preferred method for setting up a new bucket and also does not happen in Azure cloud anymore.**

Clone this repository and make sure all files are downloaded locally by running:

```
git lfs fetch --all
```

Set the

```
export ACCOUNT_NAME=temporarystoragespace
export CONTAINER_NAME=audio
```

1. Upload the files in the `data/` directory (~1GB):

```
cd data/
az storage blob upload-batch --account-name "${ACCOUNT_NAME}" -d "${CONTAINER_NAME}" -s .
```

3. Upload the youtube audio files (~50GB)

Once you have downloaded all youtube videos as audio, upload those using the script that will ignore the files already uploaded in step 1.

First run

```
./upload-missing.sh | less
```

too see a list of files that will be uploaded and then if it all looks good

```
UPLOAD=1 ./upload-missing.sh
```

to actually do the upload.

1. Upload the static files (~30GB)

Once you have downloaded the files from the two static file servers (TODO: LINK), upload the relevant directories like this:

```
az storage blob upload-batch --account-name "${ACCOUNT_NAME}" -d "${CONTAINER_NAME}/diraudio" -s static.sirimangalo.org/diraudio
az storage blob upload-batch --account-name "${ACCOUNT_NAME}" -d "${CONTAINER_NAME}/broadcast" -s share.sirimangalo.org/broadcast
```


Get and update durations:

```
python3 update_durations.py -f durations.json -d data/
python3 update_durations.py -f durations.json -d ~/audio_data/static.sirimangalo.org/diraudio/
python3 update_durations.py -f durations.json -d ~/audio_data/share.sirimangalo.org/broadcast/
python3 update_durations.py -f durations.json -d ~/audio_data/youtube/
```

`durations.json` is a mapping filename => filesize in bytes => duration in seconds

### Validation
