#!/usr/bin/env python3

import argparse
import json
import subprocess
import re
import shlex
from datetime import timedelta
from glob import glob
from os import path

parser = argparse.ArgumentParser()
parser.add_argument(
  '-d', '--directory',
  default=None,
  type=str,
  required=True,
  help="Path of directory with files to add those to duration list."
)
parser.add_argument(
  '-f', '--file',
  default=None,
  type=str,
  required=True,
  help="JSON file with durations to update."
)
args = parser.parse_args()

def get_audio_duration(filepath):
  try:
    ffprobe = subprocess.check_output('ffprobe ' + shlex.quote(filepath), stderr=subprocess.STDOUT, shell=True)
    duration = re.search(r'Duration: ([^,]+)', str(ffprobe)).group(1)
    return timedelta(hours=int(duration[:2]), minutes=int(duration[3:5]), seconds=int(float(duration[6:])))
  except Exception as e:
    print('error', e)
    return timedelta(0)

if __name__ == '__main__':
  with open(args.file) as f:
    durations = json.loads(f.read())

  for filename in glob(path.join(args.directory, '**/*.mp3'), recursive=True):
    file_base = path.basename(filename)
    if not file_base in durations:
      durations[file_base] = {}

    duration = int(get_audio_duration(filename).total_seconds())

    if duration > 0:
      durations[file_base][str(path.getsize(filename))] = duration
      print('added', file_base)

  with open(args.file, 'w') as f:
    f.write(json.dumps(durations, indent=2))
