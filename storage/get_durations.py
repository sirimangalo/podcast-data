#!/usr/bin/env python3

import json
import subprocess
import re
import shlex
from datetime import timedelta
from glob import glob
from os import path

def get_audio_duration(filepath):
    try:
        ffprobe = subprocess.check_output('ffprobe ' + shlex.quote(filepath), stderr=subprocess.STDOUT, shell=True)
        duration = re.search(r'Duration: ([^,]+)', str(ffprobe)).group(1)
        return timedelta(hours=int(duration[:2]), minutes=int(duration[3:5]), seconds=int(float(duration[6:])))
    except Exception as e:
        print('error', e)
        return timedelta(0)

if __name__ == '__main__':
    durations = { filename: int(get_audio_duration(filename).total_seconds()) for filename in glob('./data/**/*.mp3', recursive=True) }

    with open('./durations.json', 'w') as f:
        f.write(json.dumps(durations))
