# upload missing files

# provide following env variables:
#   - ACCOUNT_NAME
#   - CONTAINER_NAME
#   - UPLOAD="1" (otherwise it's a dryrun)

TEMP_DIR="/tmp"

TARGET_DIR="$1"

if [[ ! -d "${TARGET_DIR}" ]]; then
  echo "Invalid target directory"
  exit 1
fi

if [[ -z "${ACCOUNT_NAME}" ]]; then
  echo "Invalid account name"
  exit 1
fi

if [[ -z "${CONTAINER_NAME}" ]]; then
  echo "Invalid account name"
  exit 1
fi

# make working directory
temp_dir_id=$(date +%s%3N)
temp_dir="${TEMP_DIR}/audio-uploader-${temp_dir_id}"
mkdir -p "${temp_dir}"

# get a list of all blob names
file_blobs="${temp_dir}/blobs.txt"
curl -s "https://${ACCOUNT_NAME}.blob.core.windows.net/${CONTAINER_NAME}/?restype=container&comp=list" | xq -r .EnumerationResults.Blobs.Blob[].Name > "${file_blobs}"

# get a list of missing
file_offline="${temp_dir}/offline.txt"
find "${TARGET_DIR}" -type f -printf "%P\n" > "${file_offline}"

if [[ "${UPLOAD}" == "1" ]]; then
  cd "${TARGET_DIR}"
  grep -f "${file_blobs}" -v -F -x "${file_offline}" | xargs -I{} az storage blob upload --account-name "${ACCOUNT_NAME}" -c audio -f "{}" -n "{}"
else
  grep -f "${file_blobs}" -v -F -x "${file_offline}"
fi

# cleanup
rm -rf "${temp_dir}"
